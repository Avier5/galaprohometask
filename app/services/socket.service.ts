import { Injectable } from '@angular/core';

import io from 'socket.io-client';

const SERVER_URL = 'http://127.0.0.1:3000';

    @Injectable()
    export class SocketService {
        private socket;
        constructor() {
            this.socket = io.connect(SERVER_URL);
        }
       
        public send(message: Object): void {
            this.socket.emit('evt', {
                msg: message
            });
        }

        public onMessage() {
            this.socket.on('evt', (data: any) => {
                console.log(data.msg);
            });
        }
    }