import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable'

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { RequestOptions } from '@angular/http';

const httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};
 


@Injectable()
export class MainService {
    
    private apiUrl:String = 'http://127.0.0.1:3000';
    constructor(private _http:HttpClient) {}

    
    
    getURL(url, needToRedirect, onSuccess, onFailure) {
        let restUrl = this.apiUrl + '/api/url?url=' + url + "&redirect=" + needToRedirect;
        this._http.get(restUrl).subscribe(onSuccess, onFailure) ; 
    }

    getUrlFromDB(url, needToRedirect, onSuccess, onFailure) {
        let restUrl = this.apiUrl + '/api/urlFromDb?url=' + url + "&redirect=" + needToRedirect;
        this._http.get(restUrl).subscribe(onSuccess, onFailure) ; 
    }
}