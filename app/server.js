// set up ========================
var express  = require('express');
var app      = express();                               // create our app w/ express
var mongo = require('mongodb');
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)

app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
//app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
//app.use(methodOverride());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


  

// listen (start app with node server.js) ======================================

var server = app.listen(3000);
var io = require('socket.io').listen(server);
console.log("App listening on port 3000");

io.on('connection', (socket) => {
    console.log('new connection made');
    
    socket.on('event1', (data) => {
      console.log(data.msg);
    });
    
    socket.emit('event2', {
      msg: 'Server to client, do you read me? Over.'
    });
    
    socket.on('event3', (data) => {
      console.log(data.msg);
      socket.emit('event4', {
        msg: 'Loud and clear :)'
      });
    });
});
// create collection upon server init, we may need to check is collection is already in DB and not override it ======================================
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
console.log("Create a MongoDB collection");
MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("myCustomDB");
  dbo.createCollection("urls", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
  console.log("Add default URL to the collection");
  var urlObj = { url: "http://www.default.com" };
  dbo.collection("urls").insertOne(urlObj, function(err, res) {
        if (err) throw err;
        console.log("Default url inserted");
        db.close();
    });
});

app.get('/api/urlFromDb', function(req, res) {
    console.log('******** call to API /api/urlFromDb');
    var givenUrl = req.query.url;
    

    
    if(givenUrl !== '')
        return res.json("ok"); 
    else {
            MongoClient.connect(url, function(err, db) {
                if (err) throw err;
                var dbo = db.db("myCustomDB");
                dbo.collection("urls").findOne({}, function(err, result) {
                  if (err) throw err;
                  this.givenUrl = result.url;
                  db.close();
                });
                return res.json({predefinedUrl:givenUrl, errMsg: "An empty URL was sent to the server, you have been redirected to the default one"});
              }.bind(this));

              
        }
    });


app.get('/api/url', function(req, res) {
    console.log('******** call to API /api/url');
    let givenUrl = req.query.url;
    if(givenUrl !== '')
        res.json("ok"); 
    else {}
        res.json({predefinedUrl: "http://www.default.com", errMsg: "An empty URL was sent to the server, you have been redirected to the default one"});
});

