import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { NavController } from 'ionic-angular';
import { MainService } from '../../app/services/main.service';
import { SocketService } from '../../app/services/socket.service';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private urlValue = '';
  private invalideUrlMessage = '';
  private showInvalideUrlMessage = false;
  private redirectVal = false;

  constructor(public navCtrl: NavController, private _service:MainService, private _socket:SocketService) {
  }

  checkUrlAndRedirect() {
    this.invalideUrlMessage = this.urlValue === '' ? "URL can't be empty" : this.validateURL() ? "" : "URL isn't valide";
    this.showInvalideUrlMessage = this.invalideUrlMessage !== '';

    if(!this.showInvalideUrlMessage){
      window.open(this.urlValue, '_blank');
    }
  }

  onClickSubmit() {
    if(this.redirectVal)
        this.getUrlFromSocket();
//      this.getUrlFromDb();
//      this.getUrl();
    else
      this.checkUrlAndRedirect();
  }

  //Part 1
  getUrl() {
    this._service.getURL(this.urlValue, this.redirectVal, (data)=>{
      debugger;
      console.log('ok');
      if(data !== 'ok') {
        this.urlValue = data && data.predefinedUrl ? data.predefinedUrl : "";
        this.checkUrlAndRedirect();
        this.invalideUrlMessage = data && data.errMsg ? data.errMsg : "";
      } else {
        this.checkUrlAndRedirect();
      }
    }, 
    (err)=>{
      console.log('got error - server might be down')
    });
  }

  //Part 2
  getUrlFromDb() {
    this._service.getUrlFromDB(this.urlValue, this.redirectVal, (data)=>{
      debugger;
      console.log('ok');
      if(data !== 'ok') {
        this.urlValue = data && data.predefinedUrl ? data.predefinedUrl : "";
        this.checkUrlAndRedirect();
        this.invalideUrlMessage = data && data.errMsg ? data.errMsg : "";
      } else {
        this.checkUrlAndRedirect();
      }
    }, 
    (err)=>{
      console.log('got error - server might be down')
    });
  }

  //Part 3 - using a socket
  getUrlFromSocket() {
    debugger;
    this._socket.send({'url': this.urlValue, "redirect": this.redirectVal});
  }

  

  validateURL() {
    var urlPattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return urlPattern.test(this.urlValue);
  }

  

}
